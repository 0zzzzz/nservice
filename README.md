# Nservice

To launch ‘Nservice’ you need to install dependence from
requirements.txt, then run it:
$ python3 manage.py runserver
Mailing_service starts by crontab, write this in terminal to run:
$ python3 manage.py crontab add
Client API
/api/client_create/
POST
Create new client
{
"phone_number": 84796562621,
"operator_code": 479,
"client_tag": "1",
"timezone": "Asia/Macau"
}
GET
Shows all clients
/api/cl
ient_update/{client_id}/
GET
Shows current client
PUT
Edit current client
DELETE
Delite current client
Mailing list API
/api/mailing_reate/
POST
Create new mailing list
{
"mailing_date_start": "2022-01-21 00:00:00.000000",
"mailing_date_end": "2022-08-21 00:00:00.000000",
"message": "1111",
"client_filter": "1"
}
(mailing_date_end can be empty)
GET
Shows all mailing lists
/api/mailing_update/{mailing_id}/
GET
Shows current mailing list
PUT
Edit current mailing list
DELETE
Delete
Statistic List API
/api/statistic_list/?mailing_id={mailing_id}
GET
Shows mailing lists statistic in JSON by mailing id, if you don’t use parameters it’ll
shows you statistic by all mailings
