from django.db.models import Q
from django.http import Http404, JsonResponse
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Client, MailingList, Message
from .serializers import ClientSerializer, MailingListSerializer


class ClientCreateAPIView(APIView):
    def get(self, request):
        client = Client.objects.all()
        serializer = ClientSerializer(client, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = ClientSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ClientUpdateAPIView(APIView):
    def get_object(self, pk):
        try:
            return Client.objects.get(pk=pk)
        except Client.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        client = self.get_object(pk)
        serializer = ClientSerializer(client)
        return Response(serializer.data)

    def put(self, request, pk):
        client = self.get_object(pk)
        serializer = ClientSerializer(client, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        client = self.get_object(pk)
        client.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class MailingListCreateAPIView(APIView):
    def get(self, request):
        mailing = MailingList.objects.all()
        serializer = MailingListSerializer(mailing, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = MailingListSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MailingListUpdateAPIView(APIView):
    def get_object(self, pk):
        try:
            return MailingList.objects.get(pk=pk)
        except MailingList.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        mailing = self.get_object(pk)
        serializer = MailingListSerializer(mailing)
        return Response(serializer.data)

    def put(self, request, pk):
        mailing = self.get_object(pk)
        serializer = MailingListSerializer(mailing, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        mailing = self.get_object(pk)
        mailing.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class StatisticList(APIView):
    def get(self, request):
        mailing_filter = Q()
        mailing_list_id = request.GET.get('mailing_id')
        if mailing_list_id:
            mailing_filter = Q(mailing_list_id=mailing_list_id)
        messages_list = list(Message.objects.filter(mailing_filter).values().order_by('send_status'))
        messages_in_progress = Message.objects.filter(mailing_filter, Q(send_status='in_progress')).count()
        messages_overdue = Message.objects.filter(mailing_filter, Q(send_status='overdue')).count()
        messages_success = Message.objects.filter(mailing_filter, Q(send_status='success')).count()
        messages_failed = Message.objects.filter(mailing_filter, Q(send_status='failed')).count()
        final_list = [{'messages_in_progress': messages_in_progress,
                       'messages_overdue': messages_overdue,
                       'messages_success': messages_success,
                       'messages_failed': messages_failed,
                       'messages_list': messages_list}]
        return JsonResponse(final_list, safe=False)
