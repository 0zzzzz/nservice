import datetime
import pytz
import requests
from django.conf import settings
from django.db.models import Q
from mailinglist.models import MailingList, Message, Client


def remote_server_response(pk, phone, text):
    url = f'{settings.REMOTE_SERVER_URL}{pk}'
    jwt = settings.REMOTE_SERVER_JWT
    response = requests.post(url,
                             headers={'Authorization': jwt},
                             json={
                                 "id": pk,
                                 "phone": phone,
                                 "text": text
                             })
    if response.status_code == 200:
        processed_message = Message.objects.get(pk=pk)
        processed_message.send_status = Message.STATUS_SUCCESS
        processed_message.save()
    elif response.status_code == 400:
        processed_message = Message.objects.get(pk=pk)
        processed_message.send_status = Message.STATUS_FAILED
        processed_message.save()


def mailing_service():
    now = datetime.datetime.now(pytz.timezone('UTC'))
    mailing_lists = MailingList.objects.filter(
        Q(mailing_date_start__lte=now),
        Q(mailing_date_end__gte=now) | Q(mailing_date_end=None),
        Q(mailing_status=MailingList.STATUS_AWAITS)
    )
    waiting_messages = Message.objects.filter(
        mailing_list__mailing_date_end__gte=now
    )
    overdue_messages = Message.objects.filter(
        Q(mailing_list__mailing_date_end__lte=now),
        Q(send_status=Message.STATUS_IN_PROGRESS)
    )
    if mailing_lists:
        for mailing_list in mailing_lists:
            clients_list = Client.objects.filter(
                Q(client_tag=mailing_list.client_filter) | Q(operator_code=mailing_list.client_filter)
            )
            if clients_list:
                for client in clients_list:
                    new_message = Message.objects.create(
                        mailing_list=mailing_list,
                        recipient_client=client,
                        created_at=now,
                        send_status=Message.STATUS_IN_PROGRESS
                    )
                    remote_server_response(new_message.id, client.phone_number, mailing_list.message)
            mailing_list.mailing_status = MailingList.STATUS_SENDED
            mailing_list.save()
    if waiting_messages:
        for waiting_message in waiting_messages:
            remote_server_response(waiting_message.pk,
                                    waiting_message.recipient_client.phone_number,
                                    waiting_message.mailing_list.message
                                    )
    if overdue_messages:
        for overdue_message in overdue_messages:
            overdue_message.send_status = Message.STATUS_OVERDUE
