from rest_framework import serializers
from mailinglist.models import Client, MailingList


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'

    def create(self, validated_data):
        return Client.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.phone_number = validated_data.get('phone_number', instance.phone_number)
        instance.operator_code = validated_data.get('operator_code ', instance.operator_code)
        instance.client_tag = validated_data.get('client_tag', instance.client_tag)
        instance.timezone = validated_data.get('timezone', instance.timezone)
        instance.save()
        return instance


class MailingListSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailingList
        fields = '__all__'

    def create(self, validated_data):
        return MailingList.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.mailing_date_start = validated_data.get('mailing_date_start', instance.mailing_date_start)
        instance.mailing_date_end = validated_data.get('mailing_date_end', instance.mailing_date_end)
        instance.message = validated_data.get('message', instance.message)
        instance.client_filter = validated_data.get('client_filter', instance.client_filter)
        instance.mailing_status = validated_data.get('mailing_status', instance.mailing_status)
        instance.save()
        return instance
