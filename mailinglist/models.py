from django.db import models
from django.core.validators import RegexValidator
import pytz

NULLABLE = {'blank': True, 'null': True}


class MailingList(models.Model):
    STATUS_AWAITS = 'awaits'
    STATUS_CANCELED = 'canceled'
    STATUS_SENDED = 'sended'

    STATUSES = (
        (STATUS_AWAITS, 'Awaits'),
        (STATUS_CANCELED, 'Canceled'),
        (STATUS_SENDED, 'Sended'),
    )
    mailing_date_start = models.DateTimeField(verbose_name='Mailing Date Start')
    mailing_date_end = models.DateTimeField(**NULLABLE, verbose_name='Mailing Date End')
    message = models.TextField(blank=True, verbose_name='Message')
    client_filter = models.CharField(max_length=50, blank=True, verbose_name='Client Filter')
    mailing_status = models.CharField(choices=STATUSES, default=STATUS_AWAITS, verbose_name='Mailing Status',
                                      max_length=10)


class Client(models.Model):
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    phone_number_regex = RegexValidator(regex=r"^\+?1?\d{8,11}$")

    phone_number = models.CharField(validators=[phone_number_regex], max_length=11, unique=True,
                                    verbose_name='Phone Number', default=0)
    operator_code = models.PositiveIntegerField(verbose_name='Operator Code', default=0)
    client_tag = models.CharField(max_length=50, blank=True, verbose_name='Client Tag')
    timezone = models.CharField(max_length=32, choices=TIMEZONES, default='UTC')


class Message(models.Model):
    STATUS_IN_PROGRESS = 'in_progress'
    STATUS_OVERDUE = 'overdue'
    STATUS_SUCCESS = 'success'
    STATUS_FAILED = 'failed'

    STATUSES = (
        (STATUS_IN_PROGRESS, 'Processing'),
        (STATUS_OVERDUE, 'Overdue'),
        (STATUS_SUCCESS, 'Success'),
        (STATUS_FAILED, 'Fail'),
    )

    mailing_list = models.ForeignKey(MailingList, on_delete=models.CASCADE, verbose_name='Mailing list',
                                     related_name='mailing_list')
    recipient_client = models.ForeignKey(Client, on_delete=models.CASCADE, verbose_name='Recipient Client',
                                         related_name='recipient_client')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creation date')
    send_status = models.CharField(choices=STATUSES, default=STATUS_IN_PROGRESS, verbose_name='Send Status',
                                   max_length=10)
