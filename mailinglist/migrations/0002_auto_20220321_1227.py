# Generated by Django 2.2 on 2022-03-21 12:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mailinglist', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='operator_code',
            field=models.PositiveIntegerField(default=0, verbose_name='Operator Code'),
        ),
        migrations.AlterField(
            model_name='client',
            name='phone_number',
            field=models.PositiveIntegerField(default=0, verbose_name='Phone Number'),
        ),
    ]
