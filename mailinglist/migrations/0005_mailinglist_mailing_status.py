# Generated by Django 2.2 on 2022-03-21 17:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mailinglist', '0004_auto_20220321_1252'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailinglist',
            name='mailing_status',
            field=models.CharField(choices=[('awaits', 'Awaits'), ('canceled', 'Canceled'), ('sended', 'Sended')], default='awaits', max_length=10, verbose_name='Mailing Status'),
        ),
    ]
