from django.core.management import BaseCommand
from mailinglist.services.mailing_service import mailing_service


class Command(BaseCommand):

    def handle(self, *args, **options):
        mailing_service()
