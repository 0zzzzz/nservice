from django.urls import path
from mailinglist import views as views

app_name = 'mailinglist'

urlpatterns = [
    path('client_create/', views.ClientCreateAPIView.as_view(), name='client_create'),
    path('client_update/<int:pk>/', views.ClientUpdateAPIView.as_view(), name='client_update'),
    path('mailing_create/', views.MailingListCreateAPIView.as_view(), name='mailing_create'),
    path('mailing_update/<int:pk>/', views.MailingListUpdateAPIView.as_view(), name='mailing_update'),
    path('statistic_list/', views.StatisticList.as_view(), name='mailing_update'),
]
